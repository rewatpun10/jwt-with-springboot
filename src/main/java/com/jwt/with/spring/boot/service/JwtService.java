package com.jwt.with.spring.boot.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


// This is a Spring component class responsible for handling JWT (JSON Web Token) operations
// this class provides methods to generate JWTs, extract information from JWTs, and validate JWTs.
// It's commonly used for authentication and authorization in web applications
@Component
public class JwtService {

    //This constant holds the secret key used for signing the JWTs. It's used to ensure the integrity of the token
    public static final String SECRET = "5367566B59703373367639792F423F4528482B4D6251655468576D5A71347437";

    //This method generates a JWT for a given username.
    //It initializes a map of claims (which can hold additional information about the token),
    //then calls the createToken method to actually create the token
    public String generateToken(String username) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, username);
    }

    // This method constructs the JWT using the Jwts builder provided by the jjwt library.
    // It sets the claims, subject (which typically represents the user),
    // issuedAt (current timestamp), expiration (current timestamp + 30 minutes),
    // and signs the token using the HMAC SHA-256 algorithm
    private String createToken(Map<String, Object> claims, String username) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 60 * 30))
                .signWith(getSignKey(), SignatureAlgorithm.HS256).compact();
    }

    // This method decodes the secret key from Base64 and returns it as a Key object
    // that is used for signing and verifying the tokens
    private Key getSignKey() {
        byte[] keyBytes = Decoders.BASE64.decode(SECRET);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    //This method extracts the subject (username) from the JWT claims
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    //This method extracts the expiration date from the JWT claims
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    // This is a generic method to extract any claim from the JWT.
    // It takes a function that specifies which claim to extract (like subject or expiration)
    // and applies it to the claims object
    private <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    // This method parses the JWT token and returns all the claims embedded within it.
    private Claims extractAllClaims(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(getSignKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    // This method checks if the token has expired by comparing the expiration time with the current time
    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    // This method validates the token
    // by checking if the username extracted from the token matches
    // the username from the UserDetails object (typically representing the authenticated user)
    // and if the token is not expired
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
