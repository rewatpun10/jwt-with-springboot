package com.jwt.with.spring.boot.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import com.jwt.with.spring.boot.service.JwtService;
import com.jwt.with.spring.boot.service.UserInfoService;

import java.io.IOException;

// The purpose of this filter is to intercept incoming requests,
// extract JWT tokens, validate them,
// and set up Spring Security's authentication context if the token is valid.
// This way, subsequent processing of the request can depend on the user's authentication status.
@Component
public class JwtAuthFilter extends OncePerRequestFilter {
    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserInfoService userDetailsService;

    // This method is where the actual filtering logic is implemented.
    // It takes three parameters: HttpServletRequest, HttpServletResponse, and FilterChain.
    // It first extracts the JWT token from the HTTP request's Authorization header.
    // If a token is found, it extracts the username from the token using the jwtService.
    // If a username is extracted and there is no existing authentication in the SecurityContextHolder,
    // it loads the user details from the userDetailsService.
    // It then validates the token using the jwtService and, if valid,
    // creates an authentication token (UsernamePasswordAuthenticationToken) and sets it in the SecurityContextHolder.
    // Finally, it calls filterChain.doFilter(request, response) to proceed with the chain of filters for the request.
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authHeader = request.getHeader("Authorization");
        String token = null;
        String username = null;
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            token = authHeader.substring(7);
            username = jwtService.extractUsername(token);
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (jwtService.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        filterChain.doFilter(request, response);
    }
}
