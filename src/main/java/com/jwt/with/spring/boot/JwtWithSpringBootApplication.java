package com.jwt.with.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
public class JwtWithSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtWithSpringBootApplication.class, args);
	}

}
